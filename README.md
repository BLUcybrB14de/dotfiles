<img style="margin-right: 30px;" src="https://codeberg.org/repo-avatars/0556a548947bae5f2b3083d11b4f46dda83129eb5c24b7b68952d788a5b0662c" height="70" align="left">

<h1> My dotfiles for <a href="https://www.gnu.org/gnu/linux-and-gnu.html">GNU/Linux <a href="https://codeberg.org/blucybrb14de/dotfiles/"><img src="https://custom-icon-badges.demolab.com/badge/hosted%20on-codeberg-4793CC.svg?logo=codeberg&logoColor=white" alt="Codeberg badge"></a></h1>

<a href="https://codeberg.org/Coding-Liberation.Front/COPL"><img src="https://codeberg.org/Coding-Liberation.Front/COPL/raw/branch/main/images/AC-COPL-badge.webp" alt="AC-COPL-badge.webp" width="110" height="20" align="right"></a><img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub" align="right"> 
<br>
<hr>

<br>
<ul>
  <h3><li>This configuration is for my <a href="https://codeberg.org/blucybrb14de/gentoo-installguide">Gentoo</a> <a href="https://wiki.gentoo.org/wiki/Hyprland">Hyprland </a> Desktop</h3></li>
  <p> Rice inspired by <a href="https://github.com/linuxmobile/hyprland-dots">linuxmobile/hyprland-dots</a>
</ul>
<br>
<br>

<br>
<hr>

<div style=" margin: 10px; border: 2px solid black; overflow: hidden; object-fit: cover;">
   <img src="./images/Hyprland-Emacs-24.webp">
</div>
<div style=" margin: 10px; border: 2px solid black; overflow: hidden; object-fit: cover;">
   <img src="./images/Gentoo-Hyprland.webp">
</div>
</div>

<br>

<a href="https://unixporn.github.io/"><img src="https://styles.redditmedia.com/t5_2sx2i/styles/communityIcon_7fixeonxbxd41.png?width=256&s=1ecde8d0f7197fe3aa1b9d6eef5936f7401db607" alt="Unix Porn Logo" width="50" height="50" align="left" style="margin-right: 30px;"></a>

# Desktop Rice (Software Used)

<a href="https://hyprland.org"><img src="https://raw.githubusercontent.com/vaxerski/Hyprland/main/assets/header.svg" alt="Hyprland" height="150" width="400" align="right" style="margin-right: 30px;"></a>

## Window Manager

- <a href="https://github.com/hyprwm/Hyprland">Hyprland WM</a>
  - <a href="https://codeberg.org/blucybrb14de/dotfiles/src/branch/hyprland-arch/Hypr/hyprctl.conf">Config File</a>

- <a href="https://store.kde.org/browse?cat=299&ord=latest">Background</a>・<a href="https://codeberg.org/blucybrb14de/dotfiles/src/branch/hyprland-arch/backgrounds/Cutefish.webp">Cutefish.webp</a>

- <a href="https://qutebrowser.org">Qutebrowser</a>

## Shell Components

<br>
<a href="https://nwg-piotr.github.io/nwg-shell/"><img src="https://raw.githubusercontent.com/nwg-piotr/nwg-shell-resources/master/resources/logo.svg" alt="NWG Project" height="200" width="400" align="right" style="margin-right: 30px;"></a>

- <a href="https://github.com/nwg-piotr/nwg-shell">Some Components from NWG Shell are Used </a>
  - <a href="https://github.com/nwg-piotr/nwg-dock-hyprland">Nwg Dock (Hyprland)</a>
  - <a href="https://github.com/nwg-piotr/nwg-drawer">Nwg Drawer</a>

<br>

## Application launcher & Status Bar

<a href="https://wayland.freedesktop.org"><img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Wayland_Logo.svg" alt="Wayland" height="200" width="200" align="right" style="margin-right: 30px;">

<a href="https://sr.ht/~scoopta/wofi/">Wofi</a>
- <a href="https://codeberg.org/blucybrb14de/dotfiles/src/branch/gentoo-hyprland/wofi/config">Config File</a>

<a href="https://store.kde.org/browse?cat=418&ord=late">Waybar</a>
  - <a href="https://codeberg.org/blucybrb14de/dotfiles/src/branch/gentoo-hyprland/waybar/config.jsonc">Config File</a>

<br>
<br>
<hr>

<div style=" margin: 10px; border: 2px solid black; overflow: hidden; object-fit: cover;">
    <img src="./images/Qutebrowser-Emacs.webp">
</div>

# Shell Prompt & Preview

[![asciicast](https://asciinema.org/a/f4imhUUer5ifYjulXD6CYeApv.svg)](https://asciinema.org/a/f4imhUUer5ifYjulXD6CYeApv)

# Other Software that I "Use"

<a href="http://ultravioletbat.deviantart.com/art/Yay-Evil-111710573">
  <img src="https://raw.githubusercontent.com/doomemacs/doomemacs/screenshots/cacochan.png" style="margin-left: 30px;" align="right" />
</a>

<br>
<br>

| [Gentoo](https://gentoo.org) &mdash; Package Name                                   | Source                                                                                                                 |
|-----------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------:|
| [x11-drivers/nvidia-drivers](https://packages.gentoo.org/packages/x11-drivers/nvidia-drivers) (with [Wayland](https://wiki.gentoo.org/wiki/Wayland)) | [dotfiles](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/etc/X11/10-nvidia.conf)                                   |
| [app-shells/zsh](https://packages.gentoo.org/packages/app-shells/zsh) (+ [zish](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/zish) patches)                                                       | [dotfiles](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/zish-config/)・[video](https://odysee.com/@SwindlesMcCoop:1/stop-using-fish-and-start-using-zsh:2) |
| [x11-terms/alacritty](https://packages.gentoo.org/packages/x11-terms/alacritty)                                               | [logo](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/logos/alacritty.webp)・[dotfiles](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/alacritty.yml) |
| [app-misc/tmux](https://packages.gentoo.org/packages/app-misc/tmux)                                                             | [dotfiles](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/.tmux.conf)・[video](https://odysee.com/@linuxpiper:2/using-tmux:b)                                        |
| [app-editors/emacs](https://packages.gentoo.org/packages/app-editors/emacs) (+ [DOOM](https://github.com/doomemacs/doomemacs/blob/master/docs/getting_started.org#gentoo-linux)/[Emacs](https://wiki.gentoo.org/wiki/GNU_Emacs) ) | [logo](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/logos/doom-emacs.webp)・[dotfiles](https://codeberg.org/blucybrb14de/dotfiles/src/branch/main/doom)・[git](https://github.com/doomemacs/doomemacs) |

<br>

# Issues + Questions

- I will only respond to issues through <a href="https://codeberg.org/Codeberg/">Codeberg</a>
  - To report an issue for the configuration, please create <a href="https://docs.codeberg.org/getting-started/issue-tracking-basics">an issue</a>
- By reporting an issue, you are required to adhere to the Full <u><strong><a href="https://codeberg.org/Coding-Liberation/Manifesto/src/branch/main/CONTRIBUTING.md">CONTRIBUTING.md</a></strong></u>
    - This is a fork of the <a href="https://www.debian.org/code_of_conduct">Debian Code of Conduct</a>
      - Also note, the first clause & 3-6 of the <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Wolf-Pack philosphy</a></u> 
  - If at any point, you discover the solution to an issue, please <a href="https://www.funtoo.org/Wolf_Pack_Philosophy">Howl.</a><br>
- <strong>For Contact:</strong> <br><a href="https://proton.me/"><img src="https://codeberg.org/blucybrb14de/dotfiles/raw/branch/gentoo-hyprland/images/protonmail.webp"  style="vertical-align: middle" height="20" width="20"></a> blucybrb14de@coding-liberation.org

Mirrors

<a href="https://codeberg.org/blucybrb14de/dotfiles">
  <img src="https://codeberg.org/Codeberg/Design/raw/branch/main/logo/icon/svg/codeberg-logo_icon_blue.svg"
       alt="Codeberg"
       width="32">
</a>
<a href="https://gitlab.com/blucybrb14de/dotfiles">
  <img src="https://about.gitlab.com/images/press/press-kit-icon.svg"
       alt="Gitlab"
       width="32">
</a>

<hr>

<a class="tooltip" href="https://codeberg.org/Coding-Liberation.net" data-content="Coding-Liberation.net">
<img class="ui avatar gt-vm" src="https://codeberg.org/repo-avatars/be28b9fa83042b94028bf1f1b28275eebb5926f957648b5faa5842732d512e20"
title="The Coding Liberation Front - for a better future" width="28" height="28" align="left"/></a>

<strong> LICENSE  &mdash;  Unique content of this project is licensed under the <a href="https://codeberg.org/Coding-Liberation.net/COPL/src/branch/main/LICENSE">COPL.</a> </strong>


