## Forked from https://git.cbps.xyz/swindlesmccoop/zish
## Personal zconfig files located here https://codeberg.org/BLUcybrB14de/dotfiles/zish-config

# Info
Bring fish functionality to zsh for people that want to switch over but keep the convenience of fish.

# Keybinds
`Tab` = Generic tab completion for files/directories\
`<Right>` = Complete flags and arguments from history
