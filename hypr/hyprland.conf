
#    ___ ___                      .__                     .___  __________.__
#   /   |   \ ___.__._____________|  | _____    ____    __| _/  \______   \__| ____  ____
#  /    ~    <   |  |\____ \_  __ \  | \__  \  /    \  / __ |    |       _/  |/ ___\/ __ \
#  \    Y    /\___  ||  |_> >  | \/  |__/ __ \|   |  \/ /_/ |    |    |   \  \  \__\  ___/
#   \___|_  / / ____||   __/|__|  |____(____  /___|  /\____ |    |____|_  /__|\___  >___  >
#         \/  \/     |__|                   \/     \/      \/           \/        \/    \/


debug:disable_logs = false

monitor=,preferred,auto,auto

#####################################
#           Init Scripts            #
#####################################

exec-once = systemctl --user import-environment &
exec-once = hash dbus-update-activation-environment 2>/dev/null &
exec-once = dbus-update-activation-environment --systemd &
exec-once = xdg-desktop-portal-hyprland &

# Idle configuration


#####################################
#       Environment Variables       #
#####################################

# DESKTOP APPLICATION ENVIRONMENT VARIABLES

env = GDK_BACKEND=wayland
env = WAYLAND_DISPLAY=wayland-0
env = QT_QPA_PLATFORM,wayland
env = NVD_BACKEND,direct
env = QT_AUTO_SCREEN_SCALE_FACTOR,1
env = QT_WAYLAND_DISABLE_WINDOWDECORATION,1
env = QT_QPA_PLATFORMTHEME,qt5ct
env = MOZ_ENABLE_WAYLAND,1
env = STEAM_RUNTIME_PREFER_HOST_LIBRARIES,1
env = STEAM_RUNTIME,1

cursor {
       no_hardware_cursors = true
}


#####################################
#        Input Configuration        #
#####################################


input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 2 # 0|1|2|3
    float_switch_override_focus = 2

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    gaps_in = 3
    gaps_out = 5
    border_size = 3
    col.active_border = rgba(000000ee)
    layout = dwindle # master|dwindle
}

decoration {
    active_opacity = 0.98
    inactive_opacity = 1.0
    fullscreen_opacity = 1.0

    rounding = 4
    blur {
        enabled = true
        size = 15
        passes = 2 # more passes = more resource intensive.
        new_optimizations = true
        xray = true
        ignore_opacity = false
    }

    drop_shadow = false
    shadow_range = 4
    shadow_render_power = 3
    shadow_ignore_window = true

    dim_inactive = false
    col.shadow = rgba(1a1a1aee)
}

# Blur for waybar

blurls = waybar


#    _____         .__                __  .__
#   /  _  \   ____ |__| _____ _____ _/  |_|__| ____   ____   ______
#  /  /_\  \ /    \|  |/     \\__  \\   __\  |/  _ \ /    \ /  ___/
# /    |    \   |  \  |  Y Y  \/ __ \|  | |  (  <_> )   |  \\___ \
# \____|__  /___|  /__|__|_|  (____  /__| |__|\____/|___|  /____  >
#         \/     \/         \/     \/                    \/     \/



animations {
    enabled = yes
    bezier    = overshot, 0.13, 0.99, 0.29, 1.1
    animation = windows, 1, 7, overshot, slide
    animation = windowsIn, 1, 7, overshot, slide
    animation = windowsOut, 1, 7, overshot, slide
    animation = windowsMove, 1, 7, overshot, slide
    animation = border, 1, 10, default
    animation = fade, 1, 7, overshot,
    animation = workspaces, 1, 6, overshot, slidevert
}

dwindle {
    no_gaps_when_only = false
    force_split = 0
    special_scale_factor = 0.8
    split_width_multiplier = 1.0
    use_active_for_splits = true
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes
}

master {
    no_gaps_when_only = false
    special_scale_factor = 0.8
}

misc {
    disable_hyprland_logo = true
    always_follow_on_dnd = true
    layers_hog_keyboard_focus = true
    animate_manual_resizes = false
    enable_swallow = true
    swallow_regex =
    focus_on_activate = true
    vfr = 1
}

gestures {
     workspace_swipe = true
     workspace_swipe_fingers = 4
     workspace_swipe_distance = 250
     workspace_swipe_invert = true
     workspace_swipe_min_speed_to_force = 15
     workspace_swipe_cancel_ratio = 0.5
     workspace_swipe_create_new = false
}

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

#  ___ ___   ____  ____  ____       __  _    ___  __ __  ____   ____  ____   ___    ____  ____    ____  _____
# |   |   | /    ||    ||    \     |  |/ ]  /  _]|  |  ||    \ |    ||    \ |   \  |    ||    \  /    |/ ___/
# | _   _ ||  o  | |  | |  _  |    |  ' /  /  [_ |  |  ||  o  ) |  | |  _  ||    \  |  | |  _  ||   __(   \_
# |  \_/  ||     | |  | |  |  |    |    \ |    _]|  ~  ||     | |  | |  |  ||  D  | |  | |  |  ||  |  |\__  |
# |   |   ||  _  | |  | |  |  |    |     ||   [_ |___, ||  O  | |  | |  |  ||     | |  | |  |  ||  |_ |/  \ |
# |   |   ||  |  | |  | |  |  |    |  .  ||     ||     ||     | |  | |  |  ||     | |  | |  |  ||     |\    |
# |___|___||__|__||____||__|__|    |__|\_||_____||____/ |_____||____||__|__||_____||____||__|__||___,_| \___|


$mainMod = SUPER

bind = $mainMod, RETURN, exec,alacritty
bind = $mainMod, A, exec, hyprshot -m region
bind = $mainMod, Q, killactive,
bind = $mainMod SHIFT, M, exit,
bind = $mainMod, V, togglefloating,
bind = CTRL, SPACE, exec, wofi --show drun -a -W 500 -H 376
bind = $mainMod, F, fullscreen
bind = $mainMod, Y, pin
bind = $mainMod, J, togglesplit, # dwindle

bind = $mainMod, K, togglegroup,
bind = $mainMod, Tab, changegroupactive, f
bind = $mainMod SHIFT, G,exec,hyprctl --batch "keyword general:gaps_out 5;keyword general:gaps_in 3"
bind = $mainMod , G,exec,hyprctl --batch "keyword general:gaps_out 0;keyword general:gaps_in 0"

# VOLUME AND MEDIA CONTROLS

bind=,XF86AudioLowerVolume,exec,pamixer -d 3 && pamixer --get-volume > /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob
bind=,XF86AudioRaiseVolume,exec,pamixer -i 3 && pamixer --get-volume > /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob
bind=,XF86AudioMute,exec,pamixer --toggle-mute && (pamixer --get-volume || echo 0) > /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob
bind=,XF86AudioMute,exec,amixer sset Master toggle | sed -En '/\[on\]/ s/.*\[([0-9]+)%\].*/\1/ p; /\[off\]/ s/.*/0/p' | head -1 > /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob

bind=,XF86AudioPlay,exec, playerctl play-pause
bind=,XF86AudioNext,exec, playerctl next
bind=,XF86AudioPrev,exec, playerctl previous


bind = $mainMod SHIFT, P, exec, gnome-calculator
bind = CTRL, E, exec, emacsclient -c
bind = CTRL, Q, exec, qutebrowser


bind = $mainMod, L, exec, swaylock -K --fade-in 1
bind = $mainMod, O, exec, killall -SIGUSR2 waybar

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# --------------------------------------------------------------------------------------------------------------
#  __   __  ___     ______     _______   __   ___   ________  _______     __       ______    _______   ________
# |"  |/  \|  "|   /    " \   /"      \ |/"| /  ") /"       )|   __ "\   /""\     /" _  "\  /"     "| /"       )
# |'  /    \:  |  // ____  \ |:        |(: |/   / (:   \___/ (. |__) :) /    \   (: ( \___)(: ______)(:   \___/
# |: /'        | /  /    ) :)|_____/   )|    __/   \___  \   |:  ____/ /' /\  \   \/ \      \/    |   \___  \
#  \//  /\'    |(: (____/ //  //      / (// _  \    __/  \\  (|  /    //  __'  \  //  \ _   // ___)_   __/  \\
#  /   /  \\   | \        /  |:  __   \ |: | \  \  /" \   :)/|__/ \  /   /  \\  \(:   _) \ (:      "| /" \   :)
# |___/    \___|  \"_____/   |__|  \___)(__|  \__)(_______/(_______)(___/    \___)\_______) \_______)(_______/
#
# --------------------------------------------------------------------------------------------------------------


# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10
bind = $mainMod, period, workspace, e+1
bind = $mainMod, comma, workspace,e-1

bind = $mainMod, minus, movetoworkspace,special
bind = $mainMod, equal, togglespecialworkspace

bind = $mainMod SHIFT,left ,movewindow, l
bind = $mainMod SHIFT,right ,movewindow, r
bind = $mainMod SHIFT,up ,movewindow, u
bind = $mainMod SHIFT,down ,movewindow, d


#                                  _           _
#   /\/\   _____   _____  __      _(_)_ __   __| | _____      __
#  /    \ / _ \ \ / / _ \ \ \ /\ / / | '_ \ / _` |/ _ \ \ /\ / /
# / /\/\ \ (_) \ V /  __/  \ V  V /| | | | | (_| | (_) \ V  V /
# \/    \/\___/ \_/ \___|   \_/\_/ |_|_| |_|\__,_|\___/ \_/\_/


# Move active window to a workspace with mainMod + CTRL + [0-9]
bind = $mainMod CTRL, 1, movetoworkspace, 1
bind = $mainMod CTRL, 2, movetoworkspace, 2
bind = $mainMod CTRL, 3, movetoworkspace, 3
bind = $mainMod CTRL, 4, movetoworkspace, 4
bind = $mainMod CTRL, 5, movetoworkspace, 5
bind = $mainMod CTRL, 6, movetoworkspace, 6
bind = $mainMod CTRL, 7, movetoworkspace, 7
bind = $mainMod CTRL, 8, movetoworkspace, 8
bind = $mainMod CTRL, 9, movetoworkspace, 9
bind = $mainMod CTRL, 0, movetoworkspace, 10
bind = $mainMod CTRL, left, movetoworkspace, -1
bind = $mainMod CTRL, right, movetoworkspace, +1

# same as above, but doesnt switch to the workspace
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9
bind = $mainMod SHIFT, 0, movetoworkspacesilent, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

binds {
     workspace_back_and_forth = 1
     allow_workspace_cycles = 1
}
bind = $mainMod,slash,workspace,previous

bind = $mainMod,R,submap,resize
submap = resize
binde =,right,resizeactive,15 0
binde =,left,resizeactive,-15 0
binde =,up,resizeactive,0 -15
binde =,down,resizeactive,0 15
binde =,l,resizeactive,15 0
binde =,h,resizeactive,-15 0
binde =,k,resizeactive,0 -15
binde =,j,resizeactive,0 15
bind =,escape,submap,reset
submap = reset

bind=CTRL SHIFT, left, resizeactive,-15 0
bind=CTRL SHIFT, right, resizeactive,15 0
bind=CTRL SHIFT, up, resizeactive,0 -15
bind=CTRL SHIFT, down, resizeactive,0 15
bind=CTRL SHIFT, l, resizeactive, 15 0
bind=CTRL SHIFT, h, resizeactive,-15 0
bind=CTRL SHIFT, k, resizeactive, 0 -15
bind=CTRL SHIFT, j, resizeactive, 0 15

# Move/resize windows with mainMod + LMB/RMB and dragging

bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow



#-------------------------------------------------
#      ___         __             __             __
#     /   | __  __/ /_____  _____/ /_____ ______/ /_
#    / /| |/ / / / __/ __ \/ ___/ __/ __ `/ ___/ __/
#   / ___ / /_/ / /_/ /_/ (__  ) /_/ /_/ / /  / /_
#  /_/  |_\__,_/\__/\____/____/\__/\__,_/_/   \__/
#
# ------------------------------------------------

exec-once = swayidle -w timeout 300 'swaylock -f -c 000000' before-sleep 'swaylock -f -c 000000'
exec-once = hyprpaper
exec-once = waybar -c .config/waybar/config.jsonc &
exec-once = fcitx5
exec-once = blueman-applet
exec-once = mako &
exec-once = zsh -c "mkfifo /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob && tail -f /tmp/$HYPRLAND_INSTANCE_SIGNATURE.wob | wob & disown" &
exec-once = systemctl --user start hyprpolkitagent
exec-once = cbatticon
exec-once = STEAM_RUNTIME_PREFER_HOST_LIBRARIES=1 STEAM_RUNTIME=1 flatpak run com.valvesoftware.Steam -silent -vgui -nofriendsui -nochatui
exec-once = nm-applet --indicator &
exec-once = polychromatic-tray-applet
exec-once = nwg-dock-hyprland -l top



# ------------------------------------------------------------------------------------------
#     ___      __             _ _     __    __           _
#    /   \___ / _| __ _ _   _| | |_  / / /\ \ \___  _ __| | _____ _ __   __ _  ___ ___  ___
#   / /\ / _ \ |_ / _` | | | | | __| \ \/  \/ / _ \| '__| |/ / __| '_ \ / _` |/ __/ _ \/ __|
#  / /_//  __/  _| (_| | |_| | | |_   \  /\  / (_) | |  |   <\__ \ |_) | (_| | (_|  __/\__ \
# /___,' \___|_|  \__,_|\__,_|_|\__|   \/  \/ \___/|_|  |_|\_\___/ .__/ \__,_|\___\___||___/
#                                                                |_|
# ------------------------------------------------------------------------------------------



windowrulev2 = workspace 2, class:^(alacritty)$
windowrulev2 = workspace 3, class:^(emacs)$
exec-once = [workspace 2 silent] alacritty
exec-once = [workspace 3 silent] emacs

# Float Necessary Windows

windowrule=float,Rofi
windowrule=float,pavucontrol
windowrule=float,blueman-manager
windowrule=float,pcmanfm-qt
windowrule=float,polychromatic-tray-applet
windowrule=float,Discord
windowrulev2 = float,class:^()$,title:^(Picture in picture)$
windowrulev2 = float,class:^(com.github.tkashkin.gamehub)$
windowrulev2 = float,class:^(xdg-desktop-portal-hyprland)$
windowrulev2 = float,class:^(org.kde.polkit-kde-authentication-agent-1)$
windowrulev2 = float,class:^()$,title:^(Steam - Self Updater)$

# STACKING MODE by default

# Change the opacity

windowrule=opacity 0.85,qutebrowser
windowrule=opacity 0.85,brave-browser
windowrule=opacity 0.85,nwg-dock-hyprland
windowrule=opacity 0.92,alacritty
windowrule=opacity 0.85,pcmanfm-qt
windowrule=opacity 0.85,prismlauncher
windowrule=opacity 0.96,discord


#  __      __.__            .___              __________ ____ ___.____     ___________ _________
# /  \    /  \__| ____    __| _/______  _  __ \______   \    |   \    |    \_   _____//   _____/
# \   \/\/   /  |/    \  / __ |/  _ \ \/ \/ /  |       _/    |   /    |     |    __)_ \_____  \
#  \        /|  |   |  \/ /_/ (  <_> )     /   |    |   \    |  /|    |___  |        \/        \
#   \__/\  / |__|___|  /\____ |\____/ \/\_/    |____|_  /______/ |_______ \/_______  /_______  /
#        \/          \/      \/                       \/                 \/        \/        \/

#`hyprctl clients` get class、title...

windowrule=float,title:^(Picture-in-Picture)$
windowrule=size 960 540,title:^(Picture-in-Picture)$
windowrule=move 25%-,title:^(Picture-in-Picture)$
windowrule=float,imv
windowrule=move 25%-,imv
windowrule=size 960 540,imv
windowrule=float,mpv
windowrule=float,prismlauncher
windowrule=move 25%-,mpv
windowrule=size 960 540,mpv
windowrule=float,danmufloat
windowrule=move 25%-,danmufloat
windowrule=pin,danmufloat
windowrule=rounding 5,danmufloat
windowrule=size 960 540,danmufloat
windowrule=float,termfloat
windowrule=move 25%-,termfloat
windowrule=size 960 540,termfloat
windowrule=rounding 5,termfloat
windowrule=animation slide right,alacritty
windowrule=float,ncmpcpp
windowrule=move 25%-,ncmpcpp
windowrule=size 960 540,ncmpcpp
windowrule=noblur,^(waybar)$


