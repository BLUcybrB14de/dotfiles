## This is zish - a fish-inspired zsh configuration

#motd, sorta

clear

source ~/.config/zish/functions/zish-specific-settings.zsh

## echo "Welcome to zish, the zoomer fish-inspired zsh shell"


# Source personal functions and variables

source ~/.config/zish/functions/functions.zsh
source ~/.config/zish/variables.zsh
source ~/.config/zish/functions/emacs.zsh
source ~/.config/zish/functions/tmux.zsh

# PROMPT SETTINGS

clear
echo ""
echo ""
neofetch --ascii_distro arch | lolcat
fortune -s

# [Permissions]

alias pls="doas"
alias do="doas"

# Favorite Editors // I LOVE EMACS EVIL MODE AHHH!!!

alias doom="~/.config/emacs/bin/doom"
alias doomemacs="emacsclient -t -a='emacs'"
alias gmacs="emacsclient -c -a'emacs'"
alias emacs="doomemacs"

# Emacs configuration // Private Configuration for Doom EMACS

alias emacsedit="emacs ~/.config/doom/config.el"

# Common Use

alias cl="clear"
alias zish="zsh"
alias z="zish"

# Configuration Editing

alias zishedit="emacs ~/.config/zsh/.zshrc"
alias hypredit="emacs ~/.config/hypr/hyprland.conf"
alias functionedit="emacs ~/.config/zish/functions/functions.zsh"
alias varedit="emacs ~/.config/zish/variables.zsh"
alias bashedit="emacs ~/.bashrc"

alias buildedit="doas /usr/bin/vi /etc/portage/package.env"
alias provideedit="doas /usr/bin/vi /etc/portage/profile/package.provided"

# Common Editor Replacement(s)

alias nano="pls rnano"
alias vim="emacs"
alias vi="emacs"
alias .vim="doas vim"
alias .vi="doas vi"

# [UTILITIES]

# Replace some more things with better alternatives

alias eza-ls='eza --color=always --group-directories-first --icons' # preffered listing
alias eza-la='eza -a --color=always --group-directories-first --icons'  # all files and dirs
alias eza-ll='eza -l --color=always --group-directories-first --icons'  # long format
alias eza-lt='eza -aT --color=always --group-directories-first --icons' # tree listing
alias eza-l.="eza -a | egrep '^\.'"                                     # show only dotfiles

alias ls='eza-ls'
alias la='eza-la'
alias ll='eza-ll'
alias lt='eza-lt'
alias l.='eza-l.'
alias l="ls"

alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias cd.....='cd ../../../..'
alias cd......='cd ../../../../..'
alias ..='cd..'
alias ...='cd...'
alias ....='cd....'
alias .....='cd.....'
alias ......='cd......'

alias lsgrep="ls | grep"
alias rm="safe-rm"
alias cpr="cp -r"
alias dcp="doas cp"
alias scpr="dcp -r"
alias srm="doas rm"
alias srmrf="srm -rf"
alias srmdir="doas rmdir"
alias cat='bat --style header --style snip --style changes --style header'
alias top="btop"
alias htop="btop"
alias ip="ip -color"
alias xx="exit"

# Commonly used

alias genkernel="doas genkernel all"

alias update-grub="doas grub-mkconfig -o /boot/grub/grub.cfg"
alias grubup="update-grub"

alias tarnow='tar -acf '
alias untar='tar -xvf '
alias wget='wget -c '
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias hw='hwinfo --short'                          # Hardware Info
alias big="expac -H M '%m\t%n' | sort -h | nl"     # Sort installed packages according to size in MB

# Misc. Scripts

alias daddy="/usr/bin/shell-daddy"
alias doom="~/.config/emacs/bin/doom"
alias doomupgrade="doom upgrade"

alias ytfzf="ytfzf -l -s -t --ytdl-path=/usr/bin/yt-dlp --scrape=youtube,odysee,peertube --nsfw --formats --pages=2 --pages-start=1 --sort-by=relevance --notify-playing --thumb-viewer=ueberzug --thumbnail-quality=maxres"
alias yt="ytfzf"

# Development

# For Codeberg.org - stats

alias berg="~/.cargo/bin/berg"

# Git [Development] Shorthand

alias git.="git add ."

alias commita=" git commit -a"
alias push="git push"
alias pull="git pull"

alias commitpush="commita & push"
alias gitcommitpush="git. && commitpush"

alias rustc="doas rustc"
alias rustfmt="doas rustfmt"

## Z-SHELL [Command Interface] Shorthands

alias zshcopy="dcp ~/.config/zsh/.zshrc ~/.g/dotfiles/zish-config/.config/zsh/.zshrc"
alias zishcopy="zshcopy && scpr ~/.config/zish/* ~/.g/dotfiles/zish-config/.config/zish/"
alias zishupdate="zishcopy && cd ~/.g/dotfiles/ && gitcommitpush"

# System

alias zfs="doas zfs"
alias zsnapshots="zfs list -t snapshot"
alias rollback="zfs -r rollback"
alias zpool="doas zpool"

# Network

alias protonvpn="doas protonvpn"
alias protonconnect="protonvpn c"
alias protonc="protonconnect"

# System(d)

alias .systemctl="doas systemctl"
alias usystemctl="systemctl --user"
alias journalctl="doas journalctl"
