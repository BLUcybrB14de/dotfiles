(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(package-selected-packages
   '(openwith org-super-agenda evil-tutor org-download org-roam-ui org-roam org-modern)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-modern-internal-target ((t (:inherit 'default :foreground "#51afef"))))
 '(org-modern-radio-target ((t (:inherit 'default :foreground "#51afef"))))
 '(org-modern-tag ((t (:background "#2c3946" :foreground "#3f444a")))))
